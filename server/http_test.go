package server

import (
	"bytes"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler(t *testing.T) {
	avBinary = "/usr/bin/true"
	response = &ErrorResponse{
		Code:        400,
		ContentType: "text/plain",
		Body:        "file contains virus",
	}

	// prepare form data
	var emptyData bytes.Buffer
	var emptyContentType = "text/plain"

	var data bytes.Buffer
	var contentType string

	w := multipart.NewWriter(&data)
	w.CreateFormFile("file", "file_test")
	w.CreateFormField("text")
	w.Close()
	contentType = w.FormDataContentType()

	tables := []struct {
		Name         string
		Method       string
		Data         *bytes.Buffer
		ContentType  string
		ResponseCode int
	}{
		{"no body", "GET", &emptyData, "text/plain", 200},
		{"empty", "POST", &emptyData, emptyContentType, 200},
		{"multipart", "POST", &data, contentType, 400},
	}

	for _, table := range tables {
		t.Run(table.Name, func(t *testing.T) {
			req, err := http.NewRequest(table.Method, "/handler/func/?go-test-process=1&query=test", table.Data)
			if err != nil {
				t.Error(err)
			}
			req.RemoteAddr = "10.0.0.1"
			req.Header.Set("go-test-process", "1")
			req.Header.Set("Content-Type", table.ContentType)
			req.Header.Set("X-Forwarded-For", "127.0.0.1, 192.168.0.1")
			resp := httptest.NewRecorder()
			handler := http.HandlerFunc(Handler)
			handler.ServeHTTP(resp, req)

			if resp.Code != table.ResponseCode {
				t.Fail()
			}
		})
	}
}

//func TestHandlerWithEmptyBody(t *testing.T) {
//req, err := http.NewRequest("POST", "/handler/func/?go-test-process=1&query=test", nil)
//if err != nil {
//t.Error(err)
//}
//req.RemoteAddr = "10.0.0.1"
//req.Header.Add("go-test-process", "1")
//req.Header.Add("X-Forwarded-For", "127.0.0.1, 192.168.0.1")

//resp := httptest.NewRecorder()
//handler := http.HandlerFunc(Handler)
//handler.ServeHTTP(resp, req)

//t.Log(resp.Code)
//}
