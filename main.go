// ProxyAV is reverse proxy with automatic ClamAV scan for uploaded files.
package main

import (
	"fmt"

	"github.com/spf13/pflag"
	"gitlab.com/rakshazi/proxyav/server"
)

func main() {
	var port = pflag.StringP("port", "p", "8080", "ProxyAV Port")
	var target = pflag.StringP("destination", "d", "https://mockbin.com/request", "Destination URL, better to use only scheme and host, eg: https://yourhost.com")
	var clamBinary = pflag.StringP("antivirus", "a", "clamdscan", "Clam(d)scan binary path")
	var code = pflag.Int("response-code", 403, "Response HTTP Status code if file contains a virus")
	var contentType = pflag.String("response-content-type", "text/plain", "Response Content-Type if file contains a virus")
	var body = pflag.String("response-body", "Uploaded file contains a virus", "Response body content if file contains a virus")
	pflag.Parse()
	PrintBanner(*port, *target, *clamBinary)
	server.Run(*port, *target, *code, *contentType, *body, *clamBinary)
}

// Print ProxyAV ASCII-art banner, just for fun :)
func PrintBanner(port, target, clamBinary string) {
	fmt.Println()
	fmt.Println("rakshazi/")
	fmt.Println("PPPPPPPPPPPPPPPPP                                                                                       AAA")
	fmt.Println("P::::::::::::::::P                                                                                     A:::A")
	fmt.Println("P::::::PPPPPP:::::P                                                                                   A:::::A")
	fmt.Println("PP:::::P     P:::::P                                                                                 A:::::::A")
	fmt.Println("  P::::P     P:::::Prrrrr   rrrrrrrrr      ooooooooooo xxxxxxx      xxxxxxxyyyyyyy           yyyyyyyA:::::::::Avvvvvvv           vvvvvvv")
	fmt.Println("  P::::P     P:::::Pr::::rrr:::::::::r   oo:::::::::::oox:::::x    x:::::x  y:::::y         y:::::yA:::::A:::::Av:::::v         v:::::v")
	fmt.Println("  P::::PPPPPP:::::P r:::::::::::::::::r o:::::::::::::::ox:::::x  x:::::x    y:::::y       y:::::yA:::::A A:::::Av:::::v       v:::::v")
	fmt.Println("  P:::::::::::::PP  rr::::::rrrrr::::::ro:::::ooooo:::::o x:::::xx:::::x      y:::::y     y:::::yA:::::A   A:::::Av:::::v     v:::::v")
	fmt.Println("  P::::PPPPPPPPP     r:::::r     r:::::ro::::o     o::::o  x::::::::::x        y:::::y   y:::::yA:::::A     A:::::Av:::::v   v:::::v")
	fmt.Println("  P::::P             r:::::r     rrrrrrro::::o     o::::o   x::::::::x          y:::::y y:::::yA:::::AAAAAAAAA:::::Av:::::v v:::::v")
	fmt.Println("  P::::P             r:::::r            o::::o     o::::o   x::::::::x           y:::::y:::::yA:::::::::::::::::::::Av:::::v:::::v")
	fmt.Println("  P::::P             r:::::r            o::::o     o::::o  x::::::::::x           y:::::::::yA:::::AAAAAAAAAAAAA:::::Av:::::::::v")
	fmt.Println("PP::::::PP           r:::::r            o:::::ooooo:::::o x:::::xx:::::x           y:::::::yA:::::A             A:::::Av:::::::v")
	fmt.Println("P::::::::P           r:::::r            o:::::::::::::::ox:::::x  x:::::x           y:::::yA:::::A               A:::::Av:::::v")
	fmt.Println("P::::::::P           r:::::r             oo:::::::::::oox:::::x    x:::::x         y:::::yA:::::A                 A:::::Av:::v")
	fmt.Println("PPPPPPPPPP           rrrrrrr               ooooooooooo xxxxxxx      xxxxxxx       y:::::yAAAAAAA                   AAAAAAAvvv")
	fmt.Println("                                                                                 y:::::y")
	fmt.Println("                                                                                y:::::y")
	fmt.Println("                                                                               y:::::y                      powered by ClamAV")
	fmt.Println("                                                                              y:::::y")
	fmt.Println("                                                                             yyyyyyy")
	fmt.Println("Port:", port, "destination:", target, "clam(d)scan:", clamBinary)
}
